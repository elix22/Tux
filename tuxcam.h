/* Tux!
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef TUXCAM_H
#define TUXCAM_H

#include "luckey.h"


class TuxCam : public LogicComponent
{
    URHO3D_OBJECT(TuxCam, LogicComponent);
public:
    TuxCam(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);

private:
    Vector3 targetPos_;
};

#endif // TUXCAM_H
