/* Tux!
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "tuxcam.h"


TuxCam::TuxCam(Context* context) : LogicComponent(context),
    targetPos_{}
{
}

void TuxCam::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetPosition(Vector3::ONE * 10.0f);
    node_->LookAt(Vector3::UP * 5.0f);
    Camera* camera{ node_->CreateComponent<Camera>() };
    Viewport* viewport{ new Viewport(context_, GetScene(), camera) };

    RenderPath* effectRenderPath{viewport->GetRenderPath()};
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath->SetEnabled("FXAA3", true);

    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", 0.666f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2(0.8f, 0.42f));
    effectRenderPath->SetEnabled("BloomHDR", true);

    viewport->SetRenderPath(effectRenderPath);
    RENDERER->SetViewport(0, viewport);
}

void TuxCam::Update(float timeStep)
{
    Node* tuxNode{ GetScene()->GetChild("Tux") };

    if (!tuxNode)
        return;

    float oldY{ node_->GetPosition().y_ };
    targetPos_ = targetPos_.Lerp(tuxNode->GetPosition() + Vector3::UP + tuxNode->GetDirection() * 6.0f, timeStep * 2.3f);

    node_->LookAt(targetPos_ + tuxNode->GetDirection() * timeStep * 23.0f);
    node_->SetPosition(Vector3::UP * Lerp(oldY, targetPos_.y_ + 7.0f, 0.666f) + targetPos_ - node_->GetDirection().ProjectOntoPlane(Vector3::ZERO, Vector3::UP) * 23.0f);
    node_->LookAt(targetPos_);
}



