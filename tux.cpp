/* Tux!
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "tux.h"

#include "inputmaster.h"
#include "player.h"

Tux::Tux(Context* context) : Controllable(context),
    walkSpeed_{0.5f}
{
}

void Tux::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Controllable::OnNodeSet(node);

    node_->SetPosition(Vector3::UP * 0.05f + Vector3::LEFT * 5.0f);
    node_->Rotate(Quaternion(90.0f, Vector3::UP));
    model_->SetModel(CACHE->GetResource<Model>("Models/Tux.mdl"));
    model_->SetMaterial(CACHE->GetResource<Material>("Materials/VColOutline.xml"));
    model_->SetCastShadows(true);
    animCtrl_->PlayExclusive("Models/Walk.ani", 0, true);
    animCtrl_->SetStartBone("Models/Walk.ani", "Hips");

    for (bool left : {true, false}) {

        StaticModel* eye{ node_->GetChild(left ? "Eye.L" : "Eye.R", true)->CreateComponent<StaticModel>() };
        eye->SetModel(CACHE->GetResource<Model>("Models/Eye.mdl"));
        eye->ApplyMaterialList();
    }

    Player* player{ new Player(1, context_) };
    MC->GetPlayers().Push(SharedPtr<Player>(player));
    GetSubsystem<InputMaster>()->SetPlayerControl(player, this);

    SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(Tux, HandlePostRenderUpdate));
}

void Tux::Update(float timeStep)
{
    Controllable::Update(timeStep);

    Vector3 move{ Quaternion(GetScene()->GetChild("TuxCam")->GetWorldRotation().YawAngle(), Vector3::UP) * move_.ProjectOntoPlane(Vector3::ZERO, Vector3::UP) };
    walkSpeed_ = Lerp(walkSpeed_, move.Length() > 0.05f ? 1.7f : 0.42f, timeStep * 5.0f);
    animCtrl_->SetSpeed("Models/Walk.ani", 0.9f * move.Length() * walkSpeed_);

    if (move.Length() < 0.1f)
        return;

    node_->LookAt(node_->GetWorldPosition() + node_->GetDirection().Lerp(move, timeStep * 17.0f));
    node_->Translate(walkSpeed_ * move * timeStep * 2.35f, TS_WORLD);
//    node_->Rotate(Quaternion(timeStep * 17.0f, Vector3::UP));
}

void Tux::HandlePostRenderUpdate(const StringHash, VariantMap& eventData)
{
    float timeStep{ eventData[PostRenderUpdate::P_TIMESTEP].GetFloat() };

    for (bool left : {true, false}) {

        Node* eye{ node_->GetChild(left ? "Eye.L" : "Eye.R", true) };
        eye->LookAt(node_->GetWorldPosition() + node_->GetWorldDirection() * 23.0f);

        Node* cheek{ node_->GetChild(left ? "Cheek.L" : "Cheek.R", true) };
        cheek->SetScale(Vector3(1.0f, LucKey::Sine(TIME->GetElapsedTime() * 0.23f) * 0.42f + 1.0f, 1.0f));
    }

    Node* beak{ node_->GetChild("Beak", true) };
    beak->Rotate(Quaternion(timeStep * LucKey::Cosine(TIME->GetElapsedTime() * -17.0f) * 90.0f, Vector3::RIGHT));
}

